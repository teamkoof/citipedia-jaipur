package com.koof.dell.indicator.picnicSpots;

/**
 * Created by DELL on 1/12/2018.
 */

public class PicnicSpotsGetterSetter {

    private String name;
    private int thumbnail;
   // private int rating;

    public PicnicSpotsGetterSetter(){

    }

    public PicnicSpotsGetterSetter(String name, int thumbnail) {
        this.name = name;
        //this.rating = rating;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

//    public int getRating() {
//        return rating;
//    }
//
//    public void setRating(int rating) {
//        this.rating = rating;
//    }

}

