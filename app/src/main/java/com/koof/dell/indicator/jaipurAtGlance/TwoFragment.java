package com.koof.dell.indicator.jaipurAtGlance;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.koof.dell.indicator.R;

/**
 * Created by DELL on 1/7/2018.
 */

@SuppressLint("ValidFragment")
class TwoFragment extends Fragment {
    String uri = "@drawable/amber_fort1.jpg";
    ImageView imageView;

    public TwoFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_second, container, false);

        return view;
    }
}
