package com.koof.dell.indicator;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusPathAdapter extends RecyclerView.Adapter<BusPathAdapter.MyViewHolder> {

    private List<BusPathGetterSetter> BusPathList;

    public BusPathAdapter(List<BusPathGetterSetter> BusPathList) {
        this.BusPathList = BusPathList;
    }

    @Override
    public BusPathAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bus_path_map, parent, false);

        return new BusPathAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BusPathAdapter.MyViewHolder holder, int position) {
        BusPathGetterSetter busPath = BusPathList.get(position);
        holder.bus_route_name.setText(busPath.getBus_route_name());

    }

    @Override
    public int getItemCount() {
        return BusPathList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bus_route_name;

        public MyViewHolder(View view) {
            super(view);
            bus_route_name = (TextView) view.findViewById(R.id.bus_route_name);
            bus_route_name.setTextColor(Color.BLACK);

        }
    }
}
