package com.koof.dell.indicator;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusRouteAdapter extends RecyclerView.Adapter<BusRouteAdapter.MyViewHolder> {

    private List<BusRouteGetterSetter> BusRouteList;

    public BusRouteAdapter(List<BusRouteGetterSetter> BusRouteList) {
        this.BusRouteList = BusRouteList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bus_route_map, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BusRouteGetterSetter busRoute = BusRouteList.get(position);
        holder.bus_name.setText(busRoute.getBus_name());
        holder.source_destination.setText(busRoute.getSource_destination());
        holder.total_bus_stops.setText(busRoute.getTotal_bus_stops());
    }

    @Override
    public int getItemCount() {
        return BusRouteList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bus_name, source_destination, total_bus_stops;

        public MyViewHolder(View view) {
            super(view);
            bus_name = (TextView) view.findViewById(R.id.bus_name);
            bus_name.setTextColor(Color.RED);
            source_destination = (TextView) view.findViewById(R.id.source_destination);
            total_bus_stops = (TextView) view.findViewById(R.id.total_bus_stops);
        }
    }
}
