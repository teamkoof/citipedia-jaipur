package com.koof.dell.indicator;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.io.IOException;
import java.util.List;

/**
 * Created by DELL on 4/25/2017.
 */

public class BusActivity extends AppCompatActivity {

    AutoCompleteTextView autoCompleteTextView, autoCompleteTextView1;
    Button button;
    String destination,source;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus);
        button = (Button) findViewById(R.id.busSearchButton);
        BusDatabaseHandler databaseHandler = new BusDatabaseHandler(BusActivity.this);

        try {
            databaseHandler.createDataBase();
        }catch (IOException ec){
            throw new Error("Unable to create database");
        }

        try {
            databaseHandler.openDataBase();
        }catch (SQLException sqle){
            throw sqle;
        }

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.rsrtc_suggestionSource);
        autoCompleteTextView1 = (AutoCompleteTextView) findViewById(R.id.rsrtc_suggestionDestination);


        autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                loadSpinnerData();
                autoCompleteTextView.showDropDown();
                autoCompleteTextView.requestFocus();
                return false;
            }
        });
        autoCompleteTextView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                loadSpinnerData();
                autoCompleteTextView1.showDropDown();
                autoCompleteTextView1.requestFocus();
                return false;
            }
        });

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedItem= adapterView.getItemAtPosition(i).toString();;

                BusDatabaseHandler db = new BusDatabaseHandler(BusActivity.this);
                source = db.getStopId(selectedItem);

                //Log.e("source",source);
            }
        });
        autoCompleteTextView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedItem= adapterView.getItemAtPosition(i).toString();;

                BusDatabaseHandler db = new BusDatabaseHandler(BusActivity.this);
                destination = db.getStopId(selectedItem);
               // Log.e("source",destination);

            }
        });
    button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Intent intent = new Intent(BusActivity.this, BusSearch.class);
        intent.putExtra("source", source);
        intent.putExtra("destination", destination);
        startActivity(intent);

    }
});
    }
    private void loadSpinnerData() {
        // database handler
        BusDatabaseHandler db = new BusDatabaseHandler(BusActivity.this);

        // Spinner Drop down elements
        List<String> lables = db.getAllLabels();
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        autoCompleteTextView.setAdapter(dataAdapter);
        autoCompleteTextView1.setAdapter(dataAdapter);
    }

}