package com.koof.dell.indicator;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.kobakei.ratethisapp.RateThisApp;
import com.koof.dell.indicator.AutoCalculator.AutoCalculator;
import com.koof.dell.indicator.BusNewActivity.BusNewActivity;
import com.koof.dell.indicator.FlightActivity.FlightSearch;
import com.koof.dell.indicator.feedback.feedback;
import com.koof.dell.indicator.jaipurAtGlance.*;
import com.koof.dell.indicator.picnicSpots.PicnicSpotMainActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    AlertDialog.Builder builder;
    String uriString;
    GridView myGrid;
    String[] web = {
           "My Pink City", "Jaipur At Glance", "Bus", "Metro", "Train", "RSRTC", "Flight", "Auto",
            "Police Station", "Emergency Numbers", "NearBy Places", "Picnic Spots"};
    int[] imageId = {
            R.drawable.ic_location_city,R.drawable.ic_material_jaipur_glance, R.drawable.ic_bus_material, R.drawable.ic_material_metro, R.drawable.ic_material_train,
            R.drawable.ic_material_bus, R.drawable.ic_flight, R.drawable.ic_material_auto,
            R.drawable.ic_material_police_station, R.drawable.ic_material_ambulance, R.drawable.ic_material_nearby, R.drawable.ic_material_picnic
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Setting GridView
        CustomAdapter adapter = new CustomAdapter(MainActivity.this, web, imageId);
        myGrid = (GridView) findViewById(R.id.gridContentXml);
        myGrid.setAdapter(adapter);
        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(MainActivity.this, "Opening " + web[+position], Toast.LENGTH_SHORT).show();
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(MainActivity.this,floatButtonActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(MainActivity.this, com.koof.dell.indicator.jaipurAtGlance.JaipurGlance.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(MainActivity.this, BusNewActivity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(MainActivity.this, MetroActivity.class);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(MainActivity.this, TrainActivity.class);
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(MainActivity.this, RsrtcActivityNew.class);
                        startActivity(intent);
                        break;
                    case 6:
                        intent = new Intent(MainActivity.this, FlightSearch.class);
                        startActivity(intent);
                        break;
                    case 7:
                        intent = new Intent(MainActivity.this, AutoCalculator.class);
                        startActivity(intent);
                        break;
                    case 8:
                        intent = new Intent(MainActivity.this, PoliceStation.class);
                        startActivity(intent);
                        break;
                    case 9:
                        intent = new Intent(MainActivity.this, EmergencyActivity.class);
                        startActivity(intent);
                        break;

                    case 10:
                        Intent newIntent = new Intent(MainActivity.this, NearBy.class);
                        newIntent.putExtra("methodName","openPlacePicker");
                        startActivity(newIntent);
                        ;
                        break;

                    case 11:
                        intent = new Intent(MainActivity.this, PicnicSpotMainActivity.class);
                        startActivity(intent);
                        break;
                }

            }
        });


        //Setting ToolBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //setting Floating Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(MainActivity.this,floatButtonActivity.class);
               startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {

            Intent intent = new Intent(MainActivity.this, AboutUs.class);
            startActivity(intent);
        } else if (id == R.id.action_exit) {
           /* android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);*/

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Exit");
            builder.setMessage("Are you sure want to exit?");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }
            })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.create().show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_busRoute) {
            Intent intent = new Intent(MainActivity.this, BusRoute.class);
            startActivity(intent);
        } else if (id == R.id.nav_metroRoute) {
            Intent intent = new Intent(MainActivity.this, MetroActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_compass) {
            Intent intent = new Intent(MainActivity.this, CompassActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_aboutus) {
            Intent intent = new Intent(MainActivity.this, AboutUs.class);
            startActivity(intent);

        } else if (id == R.id.nav_rateus) {
            builder = new AlertDialog.Builder(MainActivity.this, AlertDialog.BUTTON_NEUTRAL);
            builder.setTitle("Thank You");
            builder.setMessage("Thank You For Using Our Application Please Give Us Your Suggestions and Feedback ");
            builder.setNegativeButton("RATE US",
                    new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog,
                int which)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.koof.dell.indicator")); // Add package name of your application
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "Thank you for your Rating",
                            Toast.LENGTH_SHORT).show();
                }
            });
            builder.setPositiveButton("QUIT",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog,
                                            int which)
                        {
                            finish();
                        }
                    });

            builder.show();

        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(MainActivity.this, feedback.class);
            startActivity(intent);

        } else if (id == R.id.nav_facebook) {
            try {
                Intent facebookIntent = new Intent(Intent.ACTION_SEND);
                facebookIntent.setType("text/plain");
                //url in uristring

                uriString = "https://play.google.com/store/apps/details?id=com.koof.dell.indicator";
                facebookIntent.putExtra(Intent.EXTRA_TEXT, uriString);
                facebookIntent.setPackage("com.facebook");
                startActivity(facebookIntent);
            } catch (Exception e) {
                Toast.makeText(this, "Please install Facebook App", Toast.LENGTH_LONG).show();
            }

        } else if (id == R.id.nav_google) {
            try {
                Intent googleIntent = new Intent(Intent.ACTION_SEND);
                googleIntent.setType("text/plain");
                //url in uristring
                googleIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.koof.dell.indicator");
                googleIntent.setPackage("com.google.android.apps.plus");
                startActivity(googleIntent);
            } catch (Exception e) {
                Toast.makeText(this, "Please install google+ App", Toast.LENGTH_LONG).show();
            }


        } else if (id == R.id.nav_instagram) {
            try {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                //url in uristring

                //uriString ="http://wwww.indicator.com";
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.koof.dell.indicator");
                whatsappIntent.setPackage("advanced.twitter.android");
                startActivity(whatsappIntent);
            } catch (Exception e) {
                Toast.makeText(this, "Please install Instagram App", Toast.LENGTH_LONG).show();
            }


        } else if (id == R.id.nav_whatsapp) {
            try {
                Intent twitterIntent = new Intent(Intent.ACTION_SEND);
                twitterIntent.setType("text/plain");
                //url in uristring

                //uriString ="http://wwww.indicator.com";
                twitterIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.koof.dell.indicator");
                twitterIntent.setPackage("com.whatsapp");
                startActivity(twitterIntent);
            } catch (Exception e) {
                Toast.makeText(this, "Please install Whatsapp App", Toast.LENGTH_LONG).show();
            }
        } else if(id == R.id.nav_work){
            Intent intent = new Intent(MainActivity.this, workWithUs.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
