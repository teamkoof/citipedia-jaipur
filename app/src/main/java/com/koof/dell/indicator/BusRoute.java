package com.koof.dell.indicator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class BusRoute extends AppCompatActivity {

    private List<BusRouteGetterSetter> BusRouteList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BusRouteAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_route);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new BusRouteAdapter(BusRouteList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

// set the adapter
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        //on click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new BusRoute.ClickListener() {

            public void onClick(View view, int position) {
                BusRouteGetterSetter busRouteGetterSetter = BusRouteList.get(position);
               String busName = busRouteGetterSetter.getBus_name();
               Log.e("BusNameee",busName);
                Intent intent = new Intent(BusRoute.this, BusPathStepper.class);

                intent.putExtra("busName", busName);
                startActivity(intent);

            }
            public void onLongClick(View view, int position) {

            }
        }));
        ;

        prepareBusRouteData();
    }
    public void prepareBusRouteData(){

        BusRouteGetterSetter busRoute1 = new BusRouteGetterSetter("Mini Bus 1", "Kumbha Marg to Khole Ke Hanuman Ji","155");
        BusRouteGetterSetter busRoute2 = new BusRouteGetterSetter("Mini Bus -2","Muslim University to Jaipur Railway Junction","16");
        BusRouteGetterSetter busRoute3 = new BusRouteGetterSetter("Mini Bus -5","Jawahar Nagar to Jaipur Railway Junction","11");
        BusRouteGetterSetter busRoute4 = new BusRouteGetterSetter("Mini Bus -7","Govindpura to Khatipura Railway Station","49");
        BusRouteGetterSetter busRoute5 = new BusRouteGetterSetter("Mini Bus-9B","Jaipur Railway Junction to Naradpura Mod","24");
        BusRouteGetterSetter busRoute6 = new BusRouteGetterSetter("Mini Bus-9B","Chandpol to Bindayaka","11");
        BusRouteGetterSetter busRoute7 = new BusRouteGetterSetter("Mini Bus-11","Benad to Bilwa","70");
        BusRouteGetterSetter busRoute8 = new BusRouteGetterSetter("Mini Bus-13","Muana Gav to Khatipura Railway Station","35");
        BusRouteGetterSetter busRoute9 = new BusRouteGetterSetter("Mini Bus-15","Dadi Ka Phatak to Todi","23");
        BusRouteGetterSetter busRoute10 = new BusRouteGetterSetter("Mini Bus-17","Bhatta Basti to Gyan Vihar College","41");
        BusRouteGetterSetter busRoute11 = new BusRouteGetterSetter("Mini Bus-21","Panchyawala to Parsotwada","27");
        BusRouteGetterSetter busRoute12 = new BusRouteGetterSetter("Mini Bus-22","Pratap Nagar to Jaisinghpura Khor","50");
        BusRouteGetterSetter busRoute13 = new BusRouteGetterSetter("Mini Bus-28","Muana Mandi to Surajpole","45");
        BusRouteGetterSetter busRoute14 = new BusRouteGetterSetter("Mini Bus-29","Mahapura to Kukas","45");
        BusRouteGetterSetter busRoute15 = new BusRouteGetterSetter("Mini Bus-30","Sarada to Paldimina","34");
        BusRouteGetterSetter busRoute16 = new BusRouteGetterSetter("Mini Bus-32","Govindpura to Kacchi Basti","40");
        BusRouteGetterSetter busRoute17 = new BusRouteGetterSetter("Mini Bus-36","Ghat Gate to Jagatpura Phatak","17");
        BusRouteGetterSetter busRoute18 = new BusRouteGetterSetter("Mini Bus-55","Mahatma Gandhi Hospital to Sikar Road","54");
        BusRouteGetterSetter busRoute19 = new BusRouteGetterSetter("Low Floor- AC1","Kukas to Sanganer","45");
        BusRouteGetterSetter busRoute20 = new BusRouteGetterSetter("Low Floor- AC2","Joshi Marg to Mahatma Gandhi Hostpital","57");
        BusRouteGetterSetter busRoute21 = new BusRouteGetterSetter("Low Floor-AC5","Amer to Agarwal Farms", "48");
        BusRouteGetterSetter busRoute22 = new BusRouteGetterSetter("Low Floor- 1","Badi Chopar to Todi","25");
        BusRouteGetterSetter busRoute23 = new BusRouteGetterSetter("Low Floor- 1A", "Sanganeri Gate to Vki Gate No 17", "23");
        BusRouteGetterSetter busRoute24 = new BusRouteGetterSetter("Low Floor- 3","Chhhoti Chopar to Dwarikapuri","38 ");
        BusRouteGetterSetter busRoute25 = new BusRouteGetterSetter("Low Floor- 3A","Chhhoti Chopar to Sanganer Town","27");
        BusRouteGetterSetter busRoute26 = new BusRouteGetterSetter("Low Floor- 3B","Chhhoti Chopar to Pannadhai Circle","29");
        BusRouteGetterSetter busRoute27 = new BusRouteGetterSetter("Low Floor- 6A","Khirni Phatak to Jawahar Circle","40");
        BusRouteGetterSetter busRoute28 = new BusRouteGetterSetter("Low Floor- 7","Transport Nagar to Panchayala Puliya", "31");
        BusRouteGetterSetter busRoute29 = new BusRouteGetterSetter("Low Floor- 8A","Chomu Puliya to Jagatpura Phatak","27");
        BusRouteGetterSetter busRoute30 = new BusRouteGetterSetter("Low Floor- 8B","Alka Cinema to Jagatpura Phatak","62");
        BusRouteGetterSetter busRoute31 = new BusRouteGetterSetter("Low Floor- 9","Govindpura to Agarwal Farm","53");
        BusRouteGetterSetter busRoute32 = new BusRouteGetterSetter("Low Floor- 9A","Dadi Ka Phatak to Agarwal Farm","42");
        BusRouteGetterSetter busRoute33 = new BusRouteGetterSetter("Low Floor- 9B", "Dharam Kanta to Mahatma Gandhi Hospital","42");
        BusRouteGetterSetter busRoute34 = new BusRouteGetterSetter("Low Floor- 10","Galta Gate to Niwaru","48");
        BusRouteGetterSetter busRoute35 = new BusRouteGetterSetter("Low Floor-10A Mini","Khole Ke Hanuman Ji to Jaipur Railway Junction","13");
        BusRouteGetterSetter busRoute36 = new BusRouteGetterSetter("Low Floor-11","Siwar to Goner", "50");
        BusRouteGetterSetter busRoute37 = new BusRouteGetterSetter("Low Floor-12","Jagatpura Phatak to Jagatpura Phatak","30");
        BusRouteGetterSetter busRoute38 = new BusRouteGetterSetter("Low Floor-14","Bassi to Jothwara Kanta","47");
        BusRouteGetterSetter busRoute39 = new BusRouteGetterSetter("Low Floor-15","Chandpol to Chomu","28");
        BusRouteGetterSetter busRoute40 = new BusRouteGetterSetter("Low Floor-16","Ajmeri Gate to Chaksu","40");
        BusRouteGetterSetter busRoute41 = new BusRouteGetterSetter("Low Floor-26","Chandpol to Bagru","33");
        BusRouteGetterSetter busRoute42= new BusRouteGetterSetter("Low Floor-27A","Goner to Vatika","47");
        BusRouteList.add(busRoute1);
        BusRouteList.add(busRoute2);
        BusRouteList.add(busRoute3);
        BusRouteList.add(busRoute4);
        BusRouteList.add(busRoute5);
        BusRouteList.add(busRoute6);
        BusRouteList.add(busRoute7);
        BusRouteList.add(busRoute8);
        BusRouteList.add(busRoute9);
        BusRouteList.add(busRoute10);
        BusRouteList.add(busRoute11);
        BusRouteList.add(busRoute12);
        BusRouteList.add(busRoute13);
        BusRouteList.add(busRoute14);
        BusRouteList.add(busRoute15);
        BusRouteList.add(busRoute16);
        BusRouteList.add(busRoute17);
        BusRouteList.add(busRoute18);
        BusRouteList.add(busRoute19);
        BusRouteList.add(busRoute20);
        BusRouteList.add(busRoute21);
        BusRouteList.add(busRoute22);
        BusRouteList.add(busRoute23);
        BusRouteList.add(busRoute24);
        BusRouteList.add(busRoute25);
        BusRouteList.add(busRoute26);
        BusRouteList.add(busRoute27);
        BusRouteList.add(busRoute28);
        BusRouteList.add(busRoute29);
        BusRouteList.add(busRoute30);
        BusRouteList.add(busRoute31);
        BusRouteList.add(busRoute32);
        BusRouteList.add(busRoute33);
        BusRouteList.add(busRoute34);
        BusRouteList.add(busRoute35);
        BusRouteList.add(busRoute36);
        BusRouteList.add(busRoute37);
        BusRouteList.add(busRoute38);
        BusRouteList.add(busRoute39);
        BusRouteList.add(busRoute40);
        BusRouteList.add(busRoute41);
        BusRouteList.add(busRoute42);

        mAdapter.notifyDataSetChanged();

    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}

