package com.koof.dell.indicator.BusNewActivity;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.koof.dell.indicator.BusDatabaseHandler;
import com.koof.dell.indicator.BusSearch;
import com.koof.dell.indicator.R;

import java.io.IOException;
import java.util.List;

/**
 * Created by DELL on 4/25/2017.
 */

public class BusNewActivity extends AppCompatActivity {

    EditText editText1,editText2;

    Button button;
    String destination,source;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_new);
        button = (Button) findViewById(R.id.busSearchButtonNew);
        BusDatabaseHandler databaseHandler = new BusDatabaseHandler(BusNewActivity.this);

        try {
            databaseHandler.createDataBase();
        }catch (IOException ec){
            throw new Error("Unable to create database");
        }

        try {
            databaseHandler.openDataBase();
        }catch (SQLException sqle){
            throw sqle;
        }
        editText1 = (EditText) findViewById(R.id.BusSearchNew);
        editText2 = (EditText) findViewById(R.id.BusSearchNew1);

        editText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BusNewActivity.this,BusNewListView.class);
                startActivityForResult(intent, 1);
            }
        });
        editText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BusNewActivity.this,BusNewListViewDestination.class);
                startActivityForResult(intent, 2);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                searchBus();

            }
        });

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {
            source=data.getStringExtra("source");
           // Toast.makeText(BusNewActivity.this,"source"+source,Toast.LENGTH_LONG).show();
            editText1.setText(source);
            BusDatabaseHandler db = new BusDatabaseHandler(BusNewActivity.this);
            source = db.getStopId(source);

        }else{
            destination=data.getStringExtra("destination");
           // Toast.makeText(BusNewActivity.this,"destination"+destination,Toast.LENGTH_LONG).show();
            editText2.setText(destination);
            BusDatabaseHandler db = new BusDatabaseHandler(BusNewActivity.this);
            destination = db.getStopId(destination);

        }
        //searchBus(destination,source);
    }

    public void searchBus(){

       // source = editText1.getText().toString();
        //destination = editText2.getText().toString();
        Intent intent = new Intent(BusNewActivity.this, BusSearch.class);
        intent.putExtra("source", source);
        intent.putExtra("destination", destination);
        startActivity(intent);

    }

}