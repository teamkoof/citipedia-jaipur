package com.koof.dell.indicator.picnicSpots;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;

import com.google.android.gms.common.api.GoogleApiClient;
import com.koof.dell.indicator.R;

import java.util.ArrayList;
import java.util.List;

public class PicnicSpotMainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PicnicSpotsAdapter adapter;
    private List<PicnicSpotsGetterSetter> PicnicSpotsGetterSetterList;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picnic_spot_activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initCollapsingToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        PicnicSpotsGetterSetterList = new ArrayList<>();
        adapter = new PicnicSpotsAdapter(this, PicnicSpotsGetterSetterList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        prepareAlbums();

        try {
            Glide.with(this).load(R.drawable.jaipurlogo).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }

       // client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Picnic Spots");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void prepareAlbums() {
        int[] posters = new int[]{

                R.drawable.city_palace_jaipur,
                R.drawable.hawa_mahal_jaipur,
                R.drawable.jantar_mantar_jaipur,
                R.drawable.sisodia_rani_ka_bagh,
                R.drawable.jaipur_zoo,
                R.drawable.jal_mahal_jaipur,
                R.drawable.amer_fort,
                R.drawable.jaigarh_fort,
                R.drawable.nahargarh_fort,
                R.drawable.kanak_vrindavan,};

        PicnicSpotsGetterSetter a = new PicnicSpotsGetterSetter("City Palace",  posters[0]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Hawa Mahal", posters[1]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Jantar Mantar Observatory",  posters[2]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Sisodia Rani Ka Bagh",  posters[3]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Jaipur Zoo",  posters[4]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Jal Mahal", posters[5]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Amer Fort", posters[6]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Jaigarh Fort",  posters[7]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Nahargarh Fort",  posters[8]);
        PicnicSpotsGetterSetterList.add(a);

        a = new PicnicSpotsGetterSetter("Kanak Vrindavan",  posters[9]);
        PicnicSpotsGetterSetterList.add(a);

        adapter.notifyDataSetChanged();
    }

//    public Action getIndexApiAction() {
//        Thing object = new Thing.Builder()
//                .setName("Main Page")
//                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
//                .build();
//        return new Action.Builder(Action.TYPE_VIEW)
//                .setObject(object)
//                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
//                .build();
//    }

    @Override
    public void onStart() {
        super.onStart();

        //client.connect();
       // AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

       // AppIndex.AppIndexApi.end(client, getIndexApiAction());
      //  client.disconnect();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            int column = position % spanCount;

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount;
                outRect.right = (column + 1) * spacing / spanCount;

                if (position < spanCount) {
                    outRect.top = spacing;
                }
                outRect.bottom = spacing;
            } else {
                outRect.left = column * spacing / spanCount;
                outRect.right = spacing - (column + 1) * spacing / spanCount;
                if (position >= spanCount) {
                    outRect.top = spacing;
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
