package com.koof.dell.indicator.BusNewActivity;

import android.content.Context;

/**
 * Created by DELL on 1/15/2018.
 */

public class BusStopGetterSetter {

    String busStopName;
    private Context mContext;

    public BusStopGetterSetter(String busStopName,Context mContext) {
        this.busStopName = busStopName;
        this.mContext = mContext;
    }

    public String getBusStopName() {
        return busStopName;
    }

    public void setBusStopName(String busStopName) {
        this.busStopName = busStopName;
    }
}
