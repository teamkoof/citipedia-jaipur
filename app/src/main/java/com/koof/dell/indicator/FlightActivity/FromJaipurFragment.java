package com.koof.dell.indicator.FlightActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.koof.dell.indicator.FlightActivityNew;
import com.koof.dell.indicator.R;

import java.util.ArrayList;
import java.util.HashMap;


public class FromJaipurFragment extends Fragment {

    String flight;
    private ListView lv;

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // Search EditText
    EditText inputSearch;


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    public FromJaipurFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_jaipur, container, false);
       final String products[] = {"Abu Dhabi - United Arab Emirates","Agra","Ahmedabad","Bangkok, Thailand","Bengaluru","Chandigarh","Chennai","Dubai - United Arab Emirates","Guwahati","Hyderabad","Indore","Jaisalmer","Jammu","Jodhpur","Kolkata","Lucknow","Mumbai","Muscat, Oman","New Delhi","Pune","Sharjah - United Arab Emirates","Singapore","Surat","Udaipur","Varanasi"};

        lv = (ListView) view.findViewById(R.id.list_view);
        inputSearch = (EditText) view.findViewById(R.id.inputSearch);
        // Adding items to listview
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, R.id.product_name, products);
        lv.setAdapter(adapter);

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                FromJaipurFragment.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //As you are using Default String Adapter
              //  Toast.makeText(getActivity(),products[position], Toast.LENGTH_SHORT).show();
                String str = products[position];
                if(str=="Delhi"){
                    flight = "DEL";
                }else if(str=="Kolkata"){
                    flight = "CCU";
                }else if(str=="Udaipur"){
                    flight = "UDR";
                }else if(str=="Jodhpur"){
                    flight = "JDH";
                }else if(str=="Aurangabad"){
                    flight = "IXU";
                } else if(str=="Mumbai"){
                    flight = "BOM";
                }else if(str=="Ahmedabad"){
                    flight = "AMD";
                }else if(str=="Abu Dhabi - United Arab Emirates") {
                    flight = "AUH";
                }else if(str=="Agra") {
                    flight = "AGR";
                }else if(str=="Bangkok, Thailand") {
                    flight = "BKK";
                }else if(str=="Chandigarh") {
                    flight = "IXC";
                }else if(str=="Chennai") {
                    flight = "MAA";
                }else if(str=="Dubai - United Arab Emirates") {
                    flight = "DXB";
                }else if(str=="Guwahati"){
                    flight = "GAU";
                }else if(str=="Hyderabad") {
                    flight = "HYD";
                }else if(str=="Indore") {
                    flight = "IDR";
                }else if(str=="Jaisalmer") {
                    flight = "JSA";
                }else if(str=="Jammu") {
                    flight = "IXJ";
                }else if(str=="Lucknow") {
                    flight = "LKO";
                }else if(str=="Muscat, Oman") {
                    flight = "MCT";
                }else if(str=="Pune") {
                    flight = "PNQ";
                }else if(str=="Sharjah - United Arab Emirates") {
                    flight = "SHJ";
                }else if(str=="Singapore") {
                    flight = "SIN";
                }else if(str=="Varanasi") {
                    flight = "VNS";
                }else if(str=="Surat") {
                    flight = "STV";
                }else if(str=="Indore") {
                    flight = "AMD";
                }else if(str=="Indore") {
                    flight = "AMD";
                }



                Intent newIntent = new Intent(getActivity(), FlightActivityNew.class);
                newIntent.putExtra("destination",flight);
                startActivity(newIntent);
            }
        });

        return view;
    }

}
