package com.koof.dell.indicator;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 12/24/2017.
 */

public class BusDatabaseHandler extends SQLiteOpenHelper {

    private static String DB_PATH = null;

    private static String DB_NAME = "database";

    private static int DB_VERSION = 2;

    private SQLiteDatabase busDatabase;

    private final Context myContext;

    public BusDatabaseHandler(Context myContext) {
        super(myContext, DB_NAME,null,DB_VERSION);
        this.myContext = myContext;
        this.DB_PATH = "/data/data/"+myContext.getPackageName()+"/"+"databases/";
        Log.e("Path 1", DB_PATH);
    }


    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    private void copyDataBase() throws IOException{
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[10];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer,0,length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH+DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch (SQLException e){

        }
        if(checkDB != null){
            checkDB.close();
        }
        return checkDB != null ?true :false;
    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH+ DB_NAME;
        busDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    public synchronized void close(){
        if(busDatabase != null)
            busDatabase.close();
        super.close();

    }




    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int newVersion, int oldVersion) {
        if(newVersion > oldVersion){
            try{
                copyDataBase();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy){
        return busDatabase.query("bus_stop_master",null,null,null,null,null,null);

    }

    public List<String> getAllLabels(){
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT  * FROM  bus_stop_master";

        busDatabase = this.getReadableDatabase();
        Cursor cursor = busDatabase.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        busDatabase.close();

        // returning lables
        return labels;
    }


    public List<String> getAllBusStops(String miniBusName){
        List<String> busStops = new ArrayList<String>();

        String selectQuery = "SELECT bus_stop_master.stop_name FROM mini_bus_name_master INNER JOIN bus_route_map ON mini_bus_name_master.bus_id = bus_route_map.bus_id INNER JOIN bus_stop_master ON bus_stop_master.stop_id = bus_route_map.stop_id WHERE mini_bus_name_master.bus_name ="+"'"+miniBusName+"'";
       // String selectQuery = "SELECT bus_stop_master.stop_name FROM bus_name_master INNER JOIN bus_route_map ON bus_name_master.bus_id = bus_route_map.bus_id INNER JOIN bus_stop_master ON bus_stop_master.stop_id = bus_route_map.stop_id WHERE bus_name_master.bus_name ="+"'"+miniBusName+"'";
      //  Log.e("Select Query for stops",selectQuery);
        busDatabase = this.getReadableDatabase();
       Cursor cursor = busDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                busStops.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        cursor.close();
        busDatabase.close();
        return busStops;
    }

    public List<String> getSearchedBusname(String Source, String Destination){
        List<String> busStops = new ArrayList<String>();

        //String selectQuery = "SELECT bus_stop_master.stop_name FROM mini_bus_name_master INNER JOIN bus_route_map ON mini_bus_name_master.bus_id = bus_route_map.bus_id INNER JOIN bus_stop_master ON bus_stop_master.stop_id = bus_route_map.stop_id WHERE mini_bus_name_master.bus_name ="+"'"+miniBusName+"'";
        String selectQuery = "SELECT mb.bus_name FROM bus_route_map br join mini_bus_name_master as mb on mb.bus_id = br.bus_id where stop_id = "+Source+ " intersect SELECT mb.bus_name FROM bus_route_map br join mini_bus_name_master as mb on mb.bus_id = br.bus_id where stop_id ="+Destination;
       // Log.e("Query ",selectQuery);
        busDatabase = this.getReadableDatabase();
        Cursor cursor = busDatabase.rawQuery(selectQuery, null);
        String result = "No Bus found for this route";
        if(cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    busStops.add(cursor.getString(0));

                } while (cursor.moveToNext());
            }
        }else{
            busStops.add(result);
        }
        cursor.close();
        busDatabase.close();
        return busStops;
    }

    public String getStopId(String stopName){
        String busStopsID = null;

        String selectQuery = "SELECT stop_id from bus_stop_master where stop_name="+"'"+stopName+"'";
        //Log.e("Query ",selectQuery);
        busDatabase = this.getReadableDatabase();
        Cursor cursor = busDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                busStopsID = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();
        busDatabase.close();
        return busStopsID;
    }

}

