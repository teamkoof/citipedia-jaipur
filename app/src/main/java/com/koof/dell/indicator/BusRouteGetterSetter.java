package com.koof.dell.indicator;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusRouteGetterSetter {

    private String bus_name, source_destination, total_bus_stops;

    public BusRouteGetterSetter(String bus_name, String source_destination, String total_bus_stops) {
        this.bus_name = bus_name;
        this.source_destination = source_destination;
        this.total_bus_stops = total_bus_stops;
    }

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getSource_destination() {
        return source_destination;
    }

    public void setSource_destination(String source_destination) {
        this.source_destination = source_destination;
    }

    public String getTotal_bus_stops() {
        return total_bus_stops;
    }

    public void setTotal_bus_stops(String total_bus_stops) {
        this.total_bus_stops = total_bus_stops;
    }
}
