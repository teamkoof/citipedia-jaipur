package com.koof.dell.indicator;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusSearchedBusAdapter extends RecyclerView.Adapter<BusSearchedBusAdapter.MyViewHolder> {

    private List<BusSearchedBusGetterSetter> searchedBusNames;
    private long[] mCurrentIds;

    public BusSearchedBusAdapter(List<BusSearchedBusGetterSetter> searchedBusNames) {
        this.searchedBusNames = searchedBusNames;
    }

    @Override
    public BusSearchedBusAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bus_path_map, parent, false);

        return new BusSearchedBusAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BusSearchedBusAdapter.MyViewHolder holder, int position) {
        BusSearchedBusGetterSetter busPath = searchedBusNames.get(position);
        holder.bus_searched_name.setText(busPath.getSearched_bus_name());

    }

    @Override
    public int getItemCount() {
        return searchedBusNames.size();
    }

    public long getItemId(int position) {
        // this will be used to get the id provided to the onItemClick callback
        return mCurrentIds[position];
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bus_searched_name;

        public MyViewHolder(View view) {
            super(view);
            bus_searched_name = (TextView) view.findViewById(R.id.bus_route_name);
            bus_searched_name.setTextColor(Color.BLACK);

        }
    }
}
