package com.koof.dell.indicator.picnicSpots;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.koof.dell.indicator.R;
import com.koof.dell.indicator.picnicSpots.Description.PicnicPlaceDescription;

import java.util.List;

/**
 * Created by DELL on 1/12/2018.
 */

public class PicnicSpotsAdapter extends RecyclerView.Adapter<PicnicSpotsAdapter.MyViewHolder> {

    private final Context mContext;
    private List<PicnicSpotsGetterSetter> PicnicSpotsGetterSetterList;

    public PicnicSpotsAdapter(Context mContext, List<PicnicSpotsGetterSetter> PicnicSpotsGetterSetterList) {
        this.mContext = mContext;
        this.PicnicSpotsGetterSetterList = PicnicSpotsGetterSetterList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            //rating= (TextView) view.findViewById(R.id.rating);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            //overflow = (ImageView) view.findViewById(R.id.overflow);
            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), PicnicPlaceDescription.class);
                    intent.putExtra("position",getAdapterPosition());
                    view.getContext().startActivity(intent);
                }
            });
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picnic_spot_card_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        PicnicSpotsGetterSetter movie = PicnicSpotsGetterSetterList.get(position);
        holder.name.setText(movie.getName());
        //holder.rating.setText("Rating: "+String.valueOf( movie.getRating()));


        Glide.with(mContext).load(movie.getThumbnail()).into(holder.thumbnail);

//        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               // showPopupMenu(holder.overflow);
//               Intent intent = new Intent()
//                Toast.makeText(view.getContext(), "the message",Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return PicnicSpotsGetterSetterList.size();
    }


}
