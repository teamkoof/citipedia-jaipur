package com.koof.dell.indicator.BusNewActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.koof.dell.indicator.BusDatabaseHandler;
import com.koof.dell.indicator.R;

import java.util.List;

public class BusNewListViewDestination extends AppCompatActivity {

    EditText editText;
    ListView listView;
    TextView textView;
    ArrayAdapter<String> dataAdapter;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_new_list_view);

        editText = (EditText) findViewById(R.id.inputSearchBusNew);
        listView = (ListView) findViewById(R.id.busNewListView);

        loadSpinnerData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String stopname = adapterView.getItemAtPosition(i).toString();
                Intent intent=new Intent();
                intent.putExtra("destination",stopname);
                setResult(2,intent);
                finish();
                //Toast.makeText(BusNewListView.this,"Hello"+" "+name,Toast.LENGTH_LONG).show();
            }
        });


    }




    private void loadSpinnerData() {
        // database handler
        BusDatabaseHandler db = new BusDatabaseHandler(BusNewListViewDestination.this);

        // Spinner Drop down elements
        List<String> lables = db.getAllLabels();
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        listView.setAdapter(dataAdapter);
        //  autoCompleteTextView.setAdapter(dataAdapter);
        // autoCompleteTextView1.setAdapter(dataAdapter);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                BusNewListViewDestination.this.dataAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }
}
