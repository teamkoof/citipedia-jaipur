package com.koof.dell.indicator;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusPathGetterSetter {

    private String bus_route_name;

    public BusPathGetterSetter(String bus_route_name) {
        this.bus_route_name = bus_route_name;
    }

    public String getBus_route_name() {
        return bus_route_name;
    }

    public void setBus_route_name(String bus_route_name) {
        this.bus_route_name = bus_route_name;
    }
}
