package com.koof.dell.indicator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

/**
 * Created by DELL on 4/25/2017.
 */

public class EmergencyActivity extends AppCompatActivity {
    String uriString;
    GridView myGrid;
    String[] web = {
            "Hospital", "Ambulance", "Fire Bridge",
            "Blood Bank", "AirPort"};
    int[] imageId = {
            R.drawable.ic_hospital,  R.drawable.ic_ambulance,
            R.drawable.ic_fire,R.drawable.ic_iv_bag,
            R.drawable.ic_airline

    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        //Setting GridView
        CustomAdapter adapter = new CustomAdapter(EmergencyActivity.this, web, imageId);
        myGrid = (GridView) findViewById(R.id.gridEmergecyXml);
        myGrid.setAdapter(adapter);
        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(EmergencyActivity.this, "Opening " + web[+position], Toast.LENGTH_SHORT).show();
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(EmergencyActivity.this, EmergencyHospital.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(EmergencyActivity.this, EmergencyAmbulance.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(EmergencyActivity.this, EmergencyFire.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(EmergencyActivity.this, EmergencyBloodbank.class);
                        startActivity(intent);
                        break;

                    case 4:
                        intent = new Intent(EmergencyActivity.this, EmergencyAirport.class);
                        startActivity(intent);
                        break;
                }

            }
        });

    }
}
