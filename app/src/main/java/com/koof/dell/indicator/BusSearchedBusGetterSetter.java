package com.koof.dell.indicator;

/**
 * Created by DELL on 12/16/2017.
 */

public class BusSearchedBusGetterSetter {

    private String searched_bus_name;

    public BusSearchedBusGetterSetter(String searched_bus_name) {
        this.searched_bus_name = searched_bus_name;
    }

    public String getSearched_bus_name() {
        return searched_bus_name;
    }

    public void setSearched_bus_name(String searched_bus_name) {
        this.searched_bus_name = searched_bus_name;
    }
}
