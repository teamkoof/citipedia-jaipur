package com.koof.dell.indicator;

import android.content.Intent;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BusSearch extends AppCompatActivity {

    private List<BusPathGetterSetter> searchedBusNames = new ArrayList<>();
    private RecyclerView recyclerView;
    private BusPathAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_search);

        BusDatabaseHandler databaseHandler = new BusDatabaseHandler(BusSearch.this);
        try {
            databaseHandler.createDataBase();
        }catch (IOException ec){
            throw new Error("Unable to create database");
        }

        try {
            databaseHandler.openDataBase();
        }catch (SQLException sqle){
            throw sqle;
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new BusPathAdapter(searchedBusNames);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

// set the adapter
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        Intent intent= getIntent();
        String source= intent.getStringExtra("source");
        String destination= intent.getStringExtra("destination");
       getBusNames(source,destination);

    }



    public void getBusNames(String source, String destination) {
        BusDatabaseHandler db = new BusDatabaseHandler(BusSearch.this);

        // Spinner Drop down elements
        List<String> lables = db.getSearchedBusname(source, destination);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, lables);

        for (int i = 0; i < dataAdapter.getCount(); i++) {
            Log.d("value adapter", dataAdapter.getItem(i));
            BusPathGetterSetter busSearchGeSe = new BusPathGetterSetter(dataAdapter.getItem(i));
            searchedBusNames.add(busSearchGeSe);
        }
    }
}
