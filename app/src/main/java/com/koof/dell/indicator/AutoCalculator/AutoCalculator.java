package com.koof.dell.indicator.AutoCalculator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.koof.dell.indicator.R;

public class AutoCalculator extends AppCompatActivity {

    EditText editText;
    TextView textView,calculationLogic;
    Button button;
    String distance;
    int fixedRate = 15;
    int distanceInt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_calculator);
        editText = (EditText) findViewById(R.id.editTextAuto);
        textView = (TextView) findViewById(R.id.TotalDiatance);
        button = (Button) findViewById(R.id.autoFareButton);
        calculationLogic = (TextView) findViewById(R.id.autoCalculationLogic);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                distance = editText.getText().toString();
               if(distance.matches("")){
                    editText.setError("Enter Distance");
               }else {
                   distanceInt = Integer.parseInt(distance);
                   int  totalDistance = (fixedRate + (distanceInt-1)*9);
                   textView.setText(Integer.toString(totalDistance));
                   Spanned html = Html.fromHtml(
                           "The Fare Calculation is done by following conditions:<br />" +
                                   "#1. For the first kilometer, <b>₹ 15 </b>is considered<br/>" +
                                   "#2. For subsequent kilometer, the fare will be calculated on <b>₹ 9 </b>per KM as base charge."
                   );
                   calculationLogic.setText(html);
                   InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                   imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
               }
                //Log.d("distanc",distanceInt);

            }
        });
    }
}
