package com.koof.dell.indicator.picnicSpots.Description;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.koof.dell.indicator.R;
import com.ms.square.android.expandabletextview.ExpandableTextView;

public class PicnicPlaceDescription extends AppCompatActivity {

    Bundle bd;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picnic_place_description);

        Intent intent = getIntent();
        bd = intent.getExtras();
        position = bd.getInt("position");
        if (position == 0) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("City Place");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.city_palace_jaipur).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.city_place));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.city_palace).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.city_palace_jaipur).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.city_place_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.city_place_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.city_place_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.city_place_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.city_place_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.city_place_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.city_place_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.city_place_duration));

        } else if (position == 1) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Hawa Mahal");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.hawa_mahal).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.hawa_mahal));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.hawa_mahal).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.hawa_mahal_jaipur).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.hawa_mahal_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.hawa_mahal_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.hawa_mahal_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.hawa_mahal_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.hawa_mahal_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.hawa_mahal_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.hawa_mahal_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.hawa_mahal_duration));

        } else if (position == 2) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Jantar Mantar");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.jantar_mantar_jaipur).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.jantar_manter));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.jantar_mantar_jaipur).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.jantar_mantar_jaipur).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.jantar_manter_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.jantar_manter_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.jantar_manter_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.jantar_manter_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.jantar_manter_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.jantar_manter_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.jantar_manter_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.jantar_manter_duration));

        } else if (position == 3) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Sisodia Rani ka Bagh");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.sisodia_rani_ka_bagh).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.sisodia_rani));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.sisodia_rani_ka_bagh).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.sisodia_rani_ka_bagh).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.sisodia_rani_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.sisodia_rani_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.sisodia_rani_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.sisodia_rani_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.sisodia_rani_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.sisodia_rani_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.sisodia_rani_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.sisodia_rani_duration));

        } else if (position == 4) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Jaipur Zoo");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.jaipur_zoo).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.jaipur_zoo));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.jaipur_zoo).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.jaipur_zoo).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.jaipur_zoo_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.jaipur_zoo_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.jaipur_zoo_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.jaipur_zoo_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.jaipur_zoo_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.jaipur_zoo_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.jaipur_zoo_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.jaipur_zoo_duration));

        } else if (position == 6) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Amber Fort");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.amer_fort).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.amber_fort));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.amer_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.amber_fort1).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.amber_fort_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.amber_fort_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.amber_fort_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.amber_fort_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.amber_fort_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.amber_fort_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.amber_fort_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.amber_fort_duration));

        }else if (position == 7) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Jaigarh Fort");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.jaigarh_fort).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.jaigarh_fort));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.jaigarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.jaigarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.jaigarh_fort_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.jaigarh_fort_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.jaigarh_fort_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.jaigarh_fort_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.jaigarh_fort_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.jaigarh_fort_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.jaigarh_fort_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.jaigarh_fort_duration));

        }else if (position == 8) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Nahargarh Fort");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.nahargarh_fort).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.nahargarh_fort));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.nahargarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.nahargarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.nahargarh_fort_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.nahargarh_fort_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.nahargarh_fort_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.nahargarh_fort_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.nahargarh_fort_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.nahargarh_fort_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.nahargarh_fort_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.nahargarh_fort_duration));

        }else if (position == 9) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Kanak Vridandavan");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.kanak_vrindavan).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.kanak_vridandavan));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.kanak_vrindavan).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.kanak_vrindavan).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.kanak_vridandavan_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.kanak_vridandavan_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.kanak_vridandavan_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.kanak_vridandavan_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.kanak_vridandavan_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.kanak_vridandavan_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.kanak_vridandavan_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.kanak_vridandavan_duration));

        }else if (position == 8) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Nahargarh Fort");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.nahargarh_fort).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.nahargarh_fort));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.nahargarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.nahargarh_fort).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.nahargarh_fort_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.nahargarh_fort_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.nahargarh_fort_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.nahargarh_fort_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.nahargarh_fort_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.nahargarh_fort_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.nahargarh_fort_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.nahargarh_fort_duration));

        }else if (position == 9) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);                          //will display the toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//will display the back arrow '<-' button
            getSupportActionBar().setTitle("Kanak Vridandavan");

            //set the image under the CollapsingToolbarLayout
            ImageView i = (ImageView) findViewById(R.id.place);
            //i.setImageResource(currentPlace.getImageResourceId());
            Glide.with(this).load(R.drawable.kanak_vrindavan).into((i));

            ExpandableTextView expTv1 = (ExpandableTextView) findViewById(R.id.expand_text_view);

            // IMPORTANT - call setText on the ExpandableTextView to set the text content to display
            expTv1.setText(getString(R.string.kanak_vridandavan));

            //to display the images in custom slider view
            SliderLayout sliderShow = (SliderLayout) findViewById(R.id.slider);
            sliderShow.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));

            //add images to slides one by one and set time duration for each slide
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView.image(R.drawable.kanak_vrindavan).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView);
            sliderShow.setDuration(1500);

            DefaultSliderView textSliderView2 = new DefaultSliderView(this);
            textSliderView2.image(R.drawable.kanak_vrindavan).setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            sliderShow.addSlider(textSliderView2);
            sliderShow.setDuration(1500);

            //set the address
            TextView addr = (TextView) findViewById(R.id.address);
            addr.setText(getString(R.string.kanak_vridandavan_address));

            //set the phone number
            LinearLayout call_layer = (LinearLayout) findViewById(R.id.call_layout);
            TextView phone = (TextView) findViewById(R.id.phone_No);
            phone.setText(getString(R.string.kanak_vridandavan_phone));
            call_layer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.kanak_vridandavan_phone)));
                    startActivity(intent);
                }
            });


            //set the website
            TextView website = (TextView) findViewById(R.id.description_website);
            website.setText(getString(R.string.kanak_vridandavan_website));

            //set the visiting hours
            TextView visintinghours = (TextView) findViewById(R.id.description_visitinghours);
            visintinghours.setText(getString(R.string.kanak_vridandavan_visitingHours));
            //set the famous for
            TextView famousfor = (TextView) findViewById(R.id.description_famousFor);
            famousfor.setText(getString(R.string.kanak_vridandavan_famousfor));

            //set the fees for
            TextView fees = (TextView) findViewById(R.id.description_fees);
            fees.setText(getString(R.string.kanak_vridandavan_fees));

            //set the fees for
            TextView duration = (TextView) findViewById(R.id.description_duration);
            duration.setText(getString(R.string.kanak_vridandavan_duration));

        }

    }

}
